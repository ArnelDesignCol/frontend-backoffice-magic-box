module.exports = {
  rules: {
    quotes: "off",
    semi: "off",
    "func-call-spacing": "off",
    indent: "off",
    "eol-last": "off",
    "no-trailing-spaces": "off",
    "space-before-blocks": "off",
    "space-before-function-paren": "off",
    "comma-dangle": "off"
  },
  extends: [
    //'plugin:vue/essential',
    '@vue/standard',
    '@vue/typescript/recommended'
  ],
  parserOptions: {
    ecmaVersion: 2020
  },
}
